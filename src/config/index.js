export default {
  /**
   * mock数据
   */
  mock: true,
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  /**
   * @API 前缀
   */
  baseURL: "/all/api/"
};
