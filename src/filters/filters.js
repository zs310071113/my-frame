import Vue from "vue";

//首字母大写
Vue.filter("firstCapitalize", function(value) {
  if (!value) return "";
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});

//银行卡等保留后四位
Vue.filter("lastShowFourNum", function(value) {
  if (!value) return "";
  var reg = /^(\d*)(\d{4})$/;
  return (value = value.replace(reg, function(a, b, c) {
    return b.replace(/\d/g, "*") + c;
  }));
});

//保留2位小数
Vue.filter("keepTwoDecimals", function(value) {
  if (!value) return "";
  return value.toFixed(2);
});
