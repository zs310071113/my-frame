const validatorRule = {
  //字符串必填
  require: [
    { required: true, type: "string", message: "请输入", trigger: "blur" }
  ],

  //num必填
  requireNumber: [
    { required: true, type: "number", message: "请输入", trigger: "blur" }
  ],

  //登陆用户
  user: [
    { required: true, message: "请输入用户名.", trigger: "blur" },
    {
      type: "string",
      pattern: /^[a-zA-Z0-9_]{4,15}$/,
      message: "用户名不合法！匹配字母数字下划线，长度位3-15",
      trigger: "blur"
    }
  ],
  //登陆密码
  password: [
    { required: true, message: "请输入密码.", trigger: "blur" },
    {
      type: "string",
      pattern: /^.{6,15}$/,
      message: "密码输入不合法！长度6-15",
      trigger: "blur"
    }
  ],
  //真实姓名
  userChineseName: [
    { required: true, message: "请输入用户名称.", trigger: "blur" },
    {
      type: "string",
      pattern: /^[\u4e00-\u9fa5]{1,20}$/,
      message: "用户名称输入不合法！汉字长度1-20",
      trigger: "blur"
    }
  ],
  //公司/单位名称
  company: [
    { required: true, message: "请输入单位/公司名称.", trigger: "blur" },
    {
      type: "string",
      pattern: /^[0-9a-zA-z\u4e00-\u9fa5]{2,20}$/,
      message: "单位/公司名称输入不合法！汉字长度4-20",
      trigger: "blur"
    }
  ],
  //选择 string与num类型
  select: [{ required: true, message: "请选择一个", trigger: "blur" }],
  selectNum: [
    { required: true, message: "请选择一个", trigger: "blur", type: "number" }
  ],
  selectDate: [
    { required: true, message: "请选择时间", trigger: "blur", type: "date" }
  ],
  //验证电话（手机与座机）
  tel: [
    { required: true, message: "请输入电话.", trigger: "blur" },
    {
      type: "string",
      pattern: /^[1][3,4,5,7,8][0-9]{9}$|^0\d{2,3}-?\d{7,8}$/,
      message: "电话输入不合法，格式不正确！",
      trigger: "blur"
    }
  ],
  //设备id任意必填字符,有长度限制,多种设备，长度不限制
  device: [
    { required: true, message: "请输入设备信息.", trigger: "blur" },
    {
      type: "string",
      pattern: /^.{6,30}$/,
      message: "设备信息输入不合法！长度6-30",
      trigger: "blur"
    }
  ],
  //时间
  date: [
    { required: true, type: "date", message: "请选择时间", trigger: "change" }
  ],
  //基础价格 保留2位数
  price: [
    { required: true, message: "请输入价格.", trigger: "blur", type: "string" },
    {
      type: "string",
      pattern: /^[1-9]+\d*(\.\d{0,2})?$|^0?\.\d{0,2}$/,
      message: "价格输入不合法！最多保留2位数.",
      trigger: "blur"
    }
  ]
};

export default validatorRule;
