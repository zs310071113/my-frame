//时间全局混淆
import * as tools from "@/libs/tools";

let mixin = {
  data() {
    return {
      startTimeOption: {},
      endTimeOption: {}
    };
  },

  methods: {
    onStartTimeChange(startTime) {
      this.endTimeOption = {
        disabledDate(endTime) {
          //return endTime < new Date(startTime);
          return endTime < new Date(startTime).getTime() - 1000 * 3600 * 9;
        }
      };
    },
    onEndTimeChange(endTime) {
      this.startTimeOption = {
        disabledDate(startTime) {
          return startTime > new Date(endTime);
        }
      };
    },

    UTC(obj, type) {
      let time = tools.getDate(new Date(obj).getTime() / 1000, type);
      return time;
    },

    // 获取当前月的第一天
    getCurrentMonthFirst() {
      var date = new Date();
      date.setDate(1);
      var month = parseInt(date.getMonth() + 1);
      var day = date.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (day < 10) {
        day = "0" + day;
      }
      return date.getFullYear() + "-" + month + "-" + day;
    },

    // 获取当前月的最后一天
    getCurrentMonthLast() {
      var date = new Date();
      var currentMonth = date.getMonth();
      var nextMonth = ++currentMonth;
      var nextMonthFirstDay = new Date(date.getFullYear(), nextMonth, 1);
      var oneDay = 1000 * 60 * 60 * 24;
      var lastTime = new Date(nextMonthFirstDay - oneDay);
      var month = parseInt(lastTime.getMonth() + 1);
      var day = lastTime.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (day < 10) {
        day = "0" + day;
      }
      return new Date(date.getFullYear() + "-" + month + "-" + day);
    },

    // 获取选择的月的第一天
    getCurrentMonthFirstA(time) {
      var date = time;
      date.setDate(1);
      var month = parseInt(date.getMonth() + 1);
      var day = date.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (day < 10) {
        day = "0" + day;
      }
      return date.getFullYear() + "-" + month + "-" + day;
    },
    // 获取选择的月的最后一天
    getCurrentMonthLastA(time) {
      var date = time;
      var currentMonth = date.getMonth();
      var nextMonth = ++currentMonth;
      var nextMonthFirstDay = new Date(date.getFullYear(), nextMonth, 1);
      var oneDay = 1000 * 60 * 60 * 24;
      var lastTime = new Date(nextMonthFirstDay - oneDay);
      var month = parseInt(lastTime.getMonth() + 1);
      var day = lastTime.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (day < 10) {
        day = "0" + day;
      }
      return new Date(date.getFullYear() + "-" + month + "-" + day);
    },

    // 获取选择的年的第一天
    getCurrentYearFirst(time) {
      var date = time;
      var Year = date.getFullYear();
      return Year + "-" + "01-01";
    },

    // 获取当前年的最后一天
    getCurrentYearLast(time) {
      var date = time;
      var currentYear = date.getFullYear();
      var nextYear = ++currentYear;
      var nextYearFirstDay = new Date(nextYear + "-" + "01-01");
      var oneDay = 1000 * 60 * 60 * 24;
      var lastTime = new Date(nextYearFirstDay - oneDay);
      var month = parseInt(lastTime.getMonth() + 1);
      var day = lastTime.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (day < 10) {
        day = "0" + day;
      }

      return date.getFullYear() + "-" + month + "-" + day;
    }
  }
};

export default mixin;
