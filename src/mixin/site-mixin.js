import { setCookieObj, getCookieObj } from "@/libs/util";
import qs from "qs";
import axios from "axios";
let mixin = {
  data() {
    return {};
  },

  methods: {
    setSiteConfig(config) {
      axios.defaults.baseURL = config.apiURL;
      axios.defaults.headers.post["Content-Type"] = config.apiType;
      if (config.apiType == "application/x-www-form-urlencoded") {
        axios.defaults.transformRequest = [
          function(data, headers) {
            return qs.stringify(data);
          }
        ];
      } else if (config.apiType == "application/json") {
        axios.defaults.transformRequest = [
          function(data, headers) {
            return JSON.stringify(data);
          }
        ];
      }
    },

    configSite(type, login, name) {
      let config = {};
      config.type = type;
      config.login = login;
      config.name = name;
      //API配置
      if (type == "xd") {
        config.apiURL = "/xd/" + name + "/api";
        config.apiType = "application/x-www-form-urlencoded";
      } else if (type == "yy") {
        config.apiURL = "/yy/" + name + "/api";
        config.apiType = "application/json";
      }
      return config;
    }
  }
};
export default mixin;
