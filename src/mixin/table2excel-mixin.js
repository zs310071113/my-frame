//导出库全局混淆
//如果没有安装， npm install --save xlsx file-saver
import FileSaver from "file-saver";
import XLSX from "xlsx";

let mixin = {
  data() {
    return {};
  },

  methods: {
    //判断渲染是否完成,z再执行导出
    exportExcelList(dom, fileName) {
      this.$nextTick(() => {
        this.exportExcel(dom, fileName);
      });
    },

    //dom是table节点， fileName是文件名 如，进销存报表数据.xlsx
    exportExcel(dom, fileName) {
      /* generate workbook object from table */
      var wb = XLSX.utils.table_to_book(document.querySelector(dom));
      /* get binary string as output */
      var wbout = XLSX.write(wb, {
        bookType: "xlsx",
        bookSST: true,
        type: "array"
      });
      try {
        FileSaver.saveAs(
          new Blob([wbout], { type: "application/octet-stream" }),
          fileName
        );
      } catch (e) {
        if (typeof console !== "undefined") console.log(e, wbout);
      }
      return wbout;
    }
  }
};

export default mixin;
