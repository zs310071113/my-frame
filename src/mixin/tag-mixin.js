//tag状态的控制
import * as util from "@/libs/util";
import { mapMutations, mapActions } from "vuex";
let mixin = {
  activated() {
    let bool = this.isCloseTag();
    if (bool) {
      if (this.resetInit) {
        this.resetInit();
      }
    }
  },

  methods: {
    ...mapMutations(["setTagNavList"]),
    isCloseTag() {
      let bool = false;
      let arr = util.getTagNavListFromLocalstorage();
      let path = this.$route.path;
      let obj = _.find(arr, { path: path });
      if (obj.status == "init") {
        bool = true;
        obj.status = "end";
        this.setTagNavList(arr);
      }
      return bool;
    }
  }
};

export default mixin;
