import Mock from "mockjs";
/* Mock.mock( rurl?, rtype?, template|function( options ) ) */
//import login from "@/view/login/mock";
//import test from "@/view/test/mock";
const registerMock = () => {
  const appContext = require.context("../view/", true, /mock\.js$/);
  let arr = [];
  appContext.keys().map(function(key) {
    let name = key.replace(/(^\.\/)|(\/mock\.js$)/g, "");
    if (appContext(key).default) {
      arr.push({ name: name, mock: appContext(key).default });
    } else {
      console.log("the mock path " + key + "  is error");
    }
  });
  return arr;
};

let mockObj = {};
let subMock = registerMock();
for (let i in subMock) {
  mockObj[subMock[i].name] = subMock[i].mock;
}

export default mockObj;
