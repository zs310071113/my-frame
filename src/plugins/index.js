import iView from "iview";
import i18n from "@/locale";
import iviewArea from "iview-area";
import MixinPlugin from "./mixin.plugin";
import MethodPlugin from "./method.plugin";
import DraggableDirectivePlugin from "./draggable.directive.plugin";

const importPlugin = Vue => {
  //引入融合语言包的iView库
  Vue.use(iView, {
    i18n: (key, value) => i18n.t(key, value)
  });
  //引入全局方法
  Vue.use(MethodPlugin);
  //引入全局插件
  Vue.use(MixinPlugin);
  //省市区全局插件
  Vue.use(iviewArea);
  //引入拖拽指令
  //Vue.use(DraggableDirectivePlugin);
};

export default importPlugin;
