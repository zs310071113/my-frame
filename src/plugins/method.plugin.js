//添加Vue全局方法,属性,实例方法 慎用 慎用 慎用!!!
import config from "@/config";
import lodash from "lodash";
export default {
  install(Vue, options) {
    /*
    Vue.test = 'test';
    Vue.testPlugin = function () {}
    Vue.prototype.$testMethod = function (methodOptions) {}
    */
    Vue.prototype.$config = config;
    Vue.prototype.$_ = lodash;
  }
};
