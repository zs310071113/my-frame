//组件全局注入 慎用 慎用 慎用!!!
import { i18nCover } from "@/libs/tools";
import { setCookieObj, getCookieObj, getSiteConfig } from "@/libs/util";
export default {
  install(Vue, options) {
    Vue.mixin({
      data: function() {
        return {};
      },

      beforeCreate() {},

      created() {
        this.mergeI18n(this.i18n);
      },

      activated() {},

      deactivated() {},

      computed: {},

      methods: {
        mergeI18n: function(i18n) {
          if (typeof i18n !== "undefined") {
            let locale = this.$i18n.locale;
            let langMessage = this.$i18n.getLocaleMessage(locale);
            if (i18n.customZhCn) {
              langMessage = i18nCover(langMessage, i18n.customZhCn);
            }
            this.$i18n.mergeLocaleMessage(locale, langMessage);
          }
        },

        getCurrentSiteConfig() {
          let obj = getSiteConfig();
          return obj;
        }
      }
    });
  }
};
