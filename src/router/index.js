import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import iView from "iview";
import {
  getCookie,
  getCookieObj,
  setCookie,
  canTurnTo,
  getSiteConfig
} from "@/libs/util";

Vue.use(Router);

const router = new Router({
  mode: "history"
});

router.customBeforeEach = function(to, from, next, configRouter) {
  iView.LoadingBar.start();
  const token = getCookie("token");
  const access = getCookieObj("access");
  const siteObj = getSiteConfig();
  const LOGIN_PAGE_NAME = siteObj ? siteObj.login : "index";
  if (to.name == "index") {
    next();
  } else if (!token && to.name !== LOGIN_PAGE_NAME) {
    next({
      name: LOGIN_PAGE_NAME
    });
  } else if (!token && to.name === LOGIN_PAGE_NAME) {
    if (siteObj) {
      next();
    } else {
      next({
        name: "index"
      });
    }
  } else if (token && to.name === LOGIN_PAGE_NAME) {
    next({
      name: siteObj.type + "-home"
    });
  } else {
    if (access && access !== "undefined") {
      if (canTurnTo(to.name, access, configRouter)) {
        next();
      } else {
        next({ replace: true, name: "error_401" });
      }
    } else {
      store.dispatch("handleClientLogOut");
      next({
        name: LOGIN_PAGE_NAME
      });
    }
  }
};

router.customAfterEach = function(to) {
  iView.LoadingBar.finish();
  window.scrollTo(0, 0);
};
export default router;
