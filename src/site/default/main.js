import Vue from "vue";
import App from "@/App.vue";
import router from "./router/routers";
import { sync } from "vuex-router-sync";
import store from "@/store";
import axios from "@/api";
import importPlugin from "@/plugins";
import i18n from "@/locale";
import "@/assets/icons/iconfont.css";
import "@/filters/filters";
import "@/styles/index.less";
import "babel-polyfill";
Vue.config.productionTip = false;
sync(store, router);
importPlugin(Vue);
new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount("#app");
