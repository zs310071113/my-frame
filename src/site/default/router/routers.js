import Main from "@/view/main/main";
import parentView from "@/components/parent-view";
import router from "@/router";
import store from "@/store";
import {getCookieObj} from "@/libs/util";

let configRouter = [
  {
    path: "/",
    name: "index",
    meta: {
      title: "引导页",
      hideInMenu: true
    },
    component: () => import("@/view/index/index.vue")
  },
  {
    path: "/main",
    name: "xd-home-management",
    meta: {
      title: "首页管理",
      hideInMenu: true,
      hideInNav: true,
      notTag: true,
      icon: "md-speedometer",
    },
    component: Main,
    children: [
      {
        path: "/xd-home",
        name: "xd-home",
        meta: {
          icon: "md-speedometer",
          title: "首页",
          hideInMenu: true,
          hideInNav: true,
          notTag: true
        },
        component: () => import("@/view/xd-home/xd-home.vue")
      }
    ]
  },
  {
    path: "/xd-login",
    name: "xd-login",
    meta: {
      title: "Login - 登录",
      hideInMenu: true,
    },
    component: () => import("@/view/xd-login/xd-login.vue")
  },
  {
    path: "/yy-login",
    name: "yy-login",
    meta: {
      title: "Login - 登录",
      hideInMenu: true,
    },
    component: () => import("@/view/yy-login/yy-login.vue")
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  },
];

let permissionConfigRouter = [
//---------------------------洗涤-------------------------//
  {
    path: "/main",
    name: "xd-settle-manage",
    meta: {
      title: "结算统计",
      icon: "md-speedometer",
      access: ["BEHQADMIN"],
    },
    component: Main,
    children: [
      {
        path: "/xd-settle",
        name: "xd-settle",
        meta: {
          icon: "md-speedometer",
          title: "结算统计",
          access: ["BEHQADMIN"],
          message: 0,
        },
        component: () => import("@/view/xd-settle/xd-settle.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-user-manage",
    meta: {
      icon: "md-contact",
      title: "用户管理",
      access: ["BEHQADMIN"],
      message: 0,
    },
    component: Main,
    children: [
      {
        path: "xd-hospital-role",
        name: "xd-hospital-role",
        meta: {
          icon: "arrow-graph-up-right",
          title: "医院账号管理",
          access: ["BEHQADMIN"],
          message: 0,
        },
        component: () => import("@/view/xd-user-manage/xd-hospital-role/xd-hospital-role.vue")
      },
      {
        path: "xd-washing-role",
        name: "xd-washing-role",
        meta: {
          icon: "arrow-graph-up-right",
          title: "洗涤账号管理",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-user-manage/xd-washing-role/xd-washing-role.vue")
      },
      {
        path: "xd-device",
        name: "xd-device",
        meta: {
          icon: "arrow-graph-up-right",
          title: "设备管理",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-user-manage/xd-device/xd-device.vue")
      },
      {
        path: "xd-hospital-wash-bind",
        name: "xd-hospital-wash-bind",
        meta: {
          icon: "arrow-graph-up-right",
          title: "医洗绑定",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-user-manage/xd-hospital-wash-bind/xd-hospital-wash-bind.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-customer-management",
    meta: {
      icon: "md-people",
      title: "合同管理",
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "xd-contract",
        name: "xd-contract",
        meta: {
          icon: "md-people",
          title: "合同管理",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-customer-management/xd-contract/xd-contract.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-wash-management",
    meta: {
      icon: "md-water",
      title: "洗涤管理",
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "xd-category",
        name: "xd-category",
        meta: {
          icon: "md-water",
          title: "价格类别",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-wash-management/xd-category/xd-category.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-label-management",
    meta: {
      icon: "md-pricetag",
      title: "标签管理",
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "xd-label-overview",
        name: "xd-label-overview",
        meta: {
          icon: "arrow-graph-up-right",
          title: "标签总览",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-label-management/xd-label-overview/xd-label-overview.vue")
      },
      {
        path: "xd-label-search",
        name: "xd-label-search",
        meta: {
          icon: "arrow-graph-up-right",
          title: "标签查询",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-label-management/xd-label-search/xd-label-search.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-wash-count",
    meta: {
      icon: "md-medal",
      title: "洗涤统计",
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "xd-put-in",
        name: "xd-put-in",
        meta: {
          icon: "arrow-graph-up-right",
          title: "入库",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-wash-count/xd-put-in/xd-put-in.vue")
      },
      {
        path: "xd-sort-out",
        name: "xd-sort-out",
        meta: {
          icon: "arrow-graph-up-right",
          title: "分拣",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-wash-count/xd-sort-out/xd-sort-out.vue")
      },
      {
        path: "xd-out-in",
        name: "xd-out-in",
        meta: {
          icon: "arrow-graph-up-right",
          title: "出库",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-wash-count/xd-out-in/xd-out-in.vue")
      }
    ]
  },
  {
    path: "/main",
    name: "xd-hospital-count",
    meta: {
      icon: "md-medkit",
      title: "医院统计",
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "xd-y-put-in",
        name: "xd-y-put-in",
        meta: {
          icon: "arrow-graph-up-right",
          title: "入库",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-hospital-count/xd-y-put-in/xd-y-put-in.vue")
      },
      {
        path: "xd-y-hand-out",
        name: "xd-y-hand-out",
        meta: {
          icon: "arrow-graph-up-right",
          title: "分发",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-hospital-count/xd-y-hand-out/xd-y-hand-out.vue")
      },
      {
        path: "xd-y-out-in",
        name: "xd-y-out-in",
        meta: {
          icon: "arrow-graph-up-right",
          title: "出库",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/xd-hospital-count/xd-y-out-in/xd-y-out-in.vue")
      }
    ]
  },

//---------------------------医院-------------------------//

  {
    path: "/main",
    name: "yy-home-management",
    meta: {
      title: "首页管理",
      hideInMenu: true,
      hideInNav: true,
      notTag:true,
      icon: "md-speedometer"
    },
    component: Main,
    children: [
      {
        path: "/yy-home",
        name: "yy-home",
        meta: {
          icon: "md-speedometer",
          title: "首页",
          hideInMenu: true,
          hideInNav: true,
          notTag: true
        },
        component: () => import("@/view/yy-home/yy-home.vue")
      }
    ]
  },

  {
    path: "/main",
    name: "yy-overview-management",
    meta: {
      title: "总概管理",
      hideInNav: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "/yy-overview",
        name: "yy-overview",
        meta: {
          icon: "md-speedometer",
          title: "总概",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/yy-overview-management/yy-overview/yy-overview.vue")
      },
    ]
  },

  {
    path: "/main",
    name: "yy-energy-management",
    meta: {
      icon: "ios-speedometer",
      title: "能源管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "yy-energy-manage",
        name: "yy-energy-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "能耗总览",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-geographic-information-system-overview",
            name: "yy-geographic-information-system-overview",
            meta: {
              icon: "arrow-graph-up-right",
              title: "GIS浏览",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-manage/yy-geographic-information-system-overview/yy-geographic-information-system-overview.vue")
          },
          {
            path: "yy-cockpit",
            name: "yy-cockpit",
            meta: {
              icon: "arrow-graph-up-right",
              title: "驾驶舱",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-manage/yy-cockpit/yy-cockpit.vue")
          }
        ]
      },

      {
        path: "yy-energy-consumption-data",
        name: "yy-energy-consumption-data",
        meta: {
          icon: "arrow-graph-up-right",
          title: "能耗数据",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-energyconsumption-query",
            name: "yy-energyconsumption-query",
            meta: {
              icon: "arrow-graph-up-right",
              title: "能耗查询",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-consumption-data/yy-energyconsumption-query/yy-energyconsumption-query.vue")
          },
          {
            path: "yy-energyconsumption-indicators",
            name: "yy-energyconsumption-indicators",
            meta: {
              icon: "arrow-graph-up-right",
              title: "能耗指标",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-consumption-data/yy-energyconsumption-indicators/yy-energyconsumption-indicators.vue")
          },
          {
            path: "yy-horizontal-contrast",
            name: "yy-horizontal-contrast",
            meta: {
              icon: "arrow-graph-up-right",
              title: "横向对比",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-consumption-data/yy-horizontal-contrast/yy-horizontal-contrast.vue")
          },
          {
            path: "yy-same-sequential-compare",
            name: "yy-same-sequential-compare",
            meta: {
              icon: "arrow-graph-up-right",
              title: "同比环比",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-consumption-data/yy-same-sequential-compare/yy-same-sequential-compare.vue")
          },
          {
            path: "yy-unit-share",
            name: "yy-unit-share",
            meta: {
              icon: "arrow-graph-up-right",
              title: "单位分摊",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-consumption-data/yy-unit-share/yy-unit-share.vue")
          },
        ]
      },
			
			{
			  path: "yy-energy-analysis-manage",
			  name: "yy-energy-analysis-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "能耗分析",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-energ-subitem",
			      name: "yy-energ-subitem",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "能耗分项",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-energy-analysis-manage/yy-energ-subitem/yy-energ-subitem.vue")
			    },
			    {
			      path: "yy-correlation-analysis",
			      name: "yy-correlation-analysis",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "关联分析",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-energy-analysis-manage/yy-correlation-analysis/yy-correlation-analysis.vue")
			    },
			    {
			      path: "yy-trend-prediction",
			      name: "yy-trend-prediction",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "趋势预测",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-energy-analysis-manage/yy-trend-prediction/yy-trend-prediction.vue")
			    },
			    {
			      path: "yy-day-night-analysis",
			      name: "yy-day-night-analysis",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "昼夜分项",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-energy-analysis-manage/yy-day-night-analysis/yy-day-night-analysis.vue")
			    },
			  ]
			},
			
			{
			  path: "yy-energy-efficiency-analysis-manage",
			  name: "yy-energy-efficiency-analysis-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "能效分析",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-energy-efficiency-query",
			      name: "yy-energy-efficiency-query",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "能效查询",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-energy-efficiency-analysis-manage/yy-energy-efficiency-query/yy-energy-efficiency-query.vue")
			    },
					{
					  path: "yy-energy-efficiency-calendar",
					  name: "yy-energy-efficiency-calendar",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "能效日历",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-energy-efficiency-analysis-manage/yy-energy-efficiency-calendar/yy-energy-efficiency-calendar.vue")
					},
					{
					  path: "yy-parameter-analysis",
					  name: "yy-parameter-analysis",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "参数分析",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-energy-efficiency-analysis-manage/yy-parameter-analysis/yy-parameter-analysis.vue")
					},
					{
					  path: "yy-energy-efficiency-contrast",
					  name: "yy-energy-efficiency-contrast",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "能效对比",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-energy-efficiency-analysis-manage/yy-energy-efficiency-contrast/yy-energy-efficiency-contrast.vue")
					},
					
			  ]
			},
			
			{
			  path: "yy-use-energy-manage",
			  name: "yy-use-energy-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "用能管理",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-energy-ranking",
			      name: "yy-energy-ranking",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "能耗排名",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-use-energy-manage/yy-energy-ranking/yy-energy-ranking.vue")
			    },
			    {
			      path: "yy-quota-manage",
			      name: "yy-quota-manage",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "定额管理",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-use-energy-manage/yy-quota-manage/yy-quota-manage.vue")
			    },
			    {
			      path: "yy-energy-examine",
			      name: "yy-energy-examine",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "能耗考核",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-use-energy-manage/yy-energy-examine/yy-energy-examine.vue")
			    },
			    {
			      path: "yy-energy-publicity",
			      name: "yy-energy-publicity",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "能耗公示",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-use-energy-manage/yy-energy-publicity/yy-energy-publicity.vue")
			    },
			  ]
			},
			
    ]
  },
	
	{
	  path: "/main",
	  name: "yy-equipment-monitoring-management",
	  meta: {
	    icon: "md-desktop",
      title: "设备监控",
      showAlways: true,
      access: ["BEHQADMIN"]
	  },
	  component: Main,
	  children: [  
	    {
	      path: "/yy-monitoring-overview-manage",
	      name: "yy-monitoring-overview-manage",
	      meta: {
	        icon: "arrow-graph-up-right",
	        title: "监控总览",
	        access: ["BEHQADMIN"]
	      },
	      component: () => import("@/view/yy-monitoring-overview-manage/yy-monitoring-overview-manage.vue")
	    },
			
			{
			  path: "yy-ransformer-distribution-system-manage",
			  name: "yy-ransformer-distribution-system-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "变配电系统",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-distribution-branch",
			      name: "yy-distribution-branch",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "配电支路",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-distribution-branch/yy-distribution-branch.vue")
			    },
			    {
			      path: "yy-electricity-room-monitoring",
			      name: "yy-electricity-room-monitoring",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "变配电室综合监控",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-electricity-room-monitoring/yy-electricity-room-monitoring.vue")
			    },
					{
					  path: "yy-high-pressure-monitoring",
					  name: "yy-high-pressure-monitoring",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "高压综合保护器",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-high-pressure-monitoring/yy-high-pressure-monitoring.vue")
					},
					{
					  path: "yy-transformer-temperature-monitoring",
					  name: "yy-transformer-temperature-monitoring",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "变压器温控器",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-transformer-temperature-monitoring/yy-transformer-temperature-monitoring.vue")
					},
					{
					  path: "yy-alternator",
					  name: "yy-alternator",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "发电机",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-alternator/yy-alternator.vue")
					},
					{
					  path: "yy-UPS",
					  name: "yy-UPS",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "UPS",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-UPS/yy-UPS.vue")
					},
					{
					  path: "yy-power-isolation-monitoring-system",
					  name: "yy-power-isolation-monitoring-system",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "电源隔离监测系统",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-power-isolation-monitoring-system/yy-power-isolation-monitoring-system.vue")
					},
					{
					  path: "yy-key-energy-branch",
					  name: "yy-key-energy-branch",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "重点用能支路",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-ransformer-distribution-system-manage/yy-key-energy-branch/yy-key-energy-branch.vue")
					},
			  ]
			},
			
			{
			  path: "yy-hvac-system-manage",
			  name: "yy-hvac-system-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "暖通空调系统",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-hvac",
			      name: "yy-hvac",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "暖通空调",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-hvac-system-manage/yy-hvac/yy-hvac.vue")
			    },
					{
					  path: "yy-direct-fired-machine",
					  name: "yy-direct-fired-machine",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "直燃机",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-hvac-system-manage/yy-direct-fired-machine/yy-direct-fired-machine.vue")
					},		    
			  ]
			},
			
			{
			  path: "yy-medical-gas-manage",
			  name: "yy-medical-gas-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "医用气体",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-air-pressure-system",
			      name: "yy-air-pressure-system",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "空压系统",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-medical-gas-manage/yy-air-pressure-system/yy-air-pressure-system.vue")
			    },
					{
					  path: "yy-liquid-oxygen-system",
					  name: "yy-liquid-oxygen-system",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "液氧系统",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-medical-gas-manage/yy-liquid-oxygen-system/yy-liquid-oxygen-system.vue")
					},
					{
					  path: "yy-negative-pressure-system",
					  name: "yy-negative-pressure-system",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "负压系统",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-medical-gas-manage/yy-negative-pressure-system/yy-negative-pressure-system.vue")
					},
					{
					  path: "yy-busbar-system",
					  name: "yy-busbar-system",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "汇流排系统",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-medical-gas-manage/yy-busbar-system/yy-busbar-system.vue")
					},
			  ]
			},
			
			{
			  path: "yy-key-areas-minitoring-manage",
			  name: "yy-key-areas-minitoring-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "重点区域监测",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-sewage-treatment-station",
			      name: "yy-sewage-treatment-station",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "污水处理站",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-key-areas-minitoring-manage/yy-sewage-treatment-station/yy-sewage-treatment-station.vue")
			    },
					{
					  path: "yy-information-center",
					  name: "yy-information-center",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "信息中心",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-key-areas-minitoring-manage/yy-information-center/yy-information-center.vue")
					},
			  ]
			},
			
			{
			  path: "yy-camera-manage",
			  name: "yy-camera-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "摄像头",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-real-time-monitoring",
			      name: "yy-real-time-monitoring",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "实时监控",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-camera-manage/yy-real-time-monitoring/yy-real-time-monitoring.vue")
			    },
					{
					  path: "yy-monitoring-playback",
					  name: "yy-monitoring-playback",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "监控回放",
					    access: ["BEHQADMIN"]
					  },
					  component: () => import("@/view/yy-camera-manage/yy-monitoring-playback/yy-monitoring-playback.vue")
					},
			  ]
			},
			
			{
			  path: "yy-clean-air-conditioning-system-manage",
			  name: "yy-clean-air-conditioning-system-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "洁净空调系统",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-clean-air-conditioning",
			      name: "yy-clean-air-conditioning",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "洁净空调",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-clean-air-conditioning-system-manage/yy-clean-air-conditioning/yy-clean-air-conditioning.vue")
			    },			    
			  ]
			},
			
			{
			  path: "yy-key-medical-equipment-manage",
			  name: "yy-key-medical-equipment-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "重点医疗设备",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-medical-equipment",
			      name: "yy-medical-equipment",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "重点医疗设备",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-key-medical-equipment-manage/yy-medical-equipment/yy-medical-equipment.vue")
			    },			    
			  ]
			},
			
			{
			  path: "yy-water-supply-drainage-system-manage",
			  name: "yy-water-supply-drainage-system-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "给排水系统",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-water-supply-drainage",
			      name: "yy-water-supply-drainage",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "给排水",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-water-supply-drainage-system-manage/yy-water-supply-drainage/yy-water-supply-drainage.vue")
			    },			    
			  ]
			},
			
			{
			  path: "yy-other-manage",
			  name: "yy-other-manage",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "其他",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [
			    {
			      path: "yy-UD-configure",
			      name: "yy-UD-configure",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "UD配置",
			        access: ["BEHQADMIN"]
			      },
			      component: () => import("@/view/yy-other-manage/yy-UD-configure/yy-UD-configure.vue")
			    },
			  ]
			},
			
	  ]
	},
	
  {
    path: "/main",
    name: "yy-logistics-management",
    meta: {
      icon: "md-construct",
      title: "后勤管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      
      {
        path: "yy-workorder-manage",
        name: "yy-workorder-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "工单管理",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-gd-repair-management",
            name: "yy-gd-repair-management",
            meta: {
              icon: "arrow-graph-up-right",
              title: "工单报修与管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-gd-repair-management/yy-gd-repair-management.vue")
          },
          {
            path: "yy-gd-accept-deal-manage",
            name: "yy-gd-accept-deal-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "工单受理管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-gd-accept-deal-manage/yy-gd-accept-deal-manage.vue")
          },
          {
            path: "yy-gd-receive-order-manage",
            name: "yy-gd-receive-order-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "工单接单管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-gd-receive-order-manage/yy-gd-receive-order-manage.vue")
          },
          {
            path: "yy-gd-perform-manage",
            name: "yy-gd-perform-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "工单执行管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-gd-perform-manage/yy-gd-perform-manage.vue")
          },
          {
            path: "yy-gd-Integrated-query",
            name: "yy-gd-Integrated-query",
            meta: {
              icon: "arrow-graph-up-right",
              title: "工单综合查询",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-gd-Integrated-query/yy-gd-Integrated-query.vue")
          },
          {
            path: "yy-screen-show",
            name: "yy-screen-show",
            meta: {
              icon: "arrow-graph-up-right",
              title: "大屏展示",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-screen-show/yy-screen-show.vue")
          },
          {
            path: "yy-scheduling-screen-show",
            name: "yy-scheduling-screen-show",
            meta: {
              icon: "arrow-graph-up-right",
              title: "调度大屏展示",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-workorder-manage/yy-scheduling-screen-show/yy-scheduling-screen-show.vue")
          },
        ]
      },
      

      {
        path: "yy-warehouse-manage",
        name: "yy-warehouse-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "工程仓库",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-supplier-manage",
            name: "yy-supplier-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "供应商管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-supplier-manage/yy-supplier-manage.vue")
          },
          {
            path: "yy-goods-cateogory",
            name: "yy-goods-cateogory",
            meta: {
              icon: "arrow-graph-up-right",
              title: "物品类别",
              access: ["BEHQADMIN"]
          },
            component: () => import("@/view/yy-warehouse-manage/yy-goods-cateogory/yy-goods-cateogory.vue")
          },
          {
            path: "yy-goods-manage",
            name: "yy-goods-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "物品管理",
              access: ["BEHQADMIN"]
          },
            component: () => import("@/view/yy-warehouse-manage/yy-goods-manage/yy-goods-manage.vue")
          },
          {
            path: "yy-put-in-manage",
            name: "yy-put-in-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "入库管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-put-in-manage/yy-put-in-manage.vue")
          },
          {
            path: "yy-out-in-manage",
            name: "yy-out-in-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "出库管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-out-in-manage/yy-out-in-manage.vue")
          },
          {
            path: "yy-store-list",
            name: "yy-store-list",
            meta: {
              icon: "arrow-graph-up-right",
              title: "库存清单",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-store-list/yy-store-list.vue")
          },
          {
            path: "yy-parameter-record",
            name: "yy-parameter-record",
            meta: {
              icon: "arrow-graph-up-right",
              title: "台账记录",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-parameter-record/yy-parameter-record.vue")
          },
          {
            path: "yy-sell-store-report",
            name: "yy-sell-store-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "进销存报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-sell-store-report/yy-sell-store-report.vue")
          },
          {
            path: "yy-month-reports",
            name: "yy-month-reports",
            meta: {
              icon: "arrow-graph-up-right",
              title: "月度结转",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-warehouse-manage/yy-month-reports/yy-month-reports.vue")
          }
        ]
      },
      
      {
        path: "yy-dangerouschemicals-manage",
        name: "yy-dangerouschemicals-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "危化品管理",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-whp-supplier-manage",
            name: "yy-whp-supplier-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "供应商管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-supplier-manage/yy-whp-supplier-manage.vue")
          },
          {
            path: "yy-whp-goods-cateogory",
            name: "yy-whp-goods-cateogory",
            meta: {
              icon: "arrow-graph-up-right",
              title: "危化品类别",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-goods-cateogory/yy-whp-goods-cateogory.vue")
          },
          {
            path: "yy-whp-goods-manage",
            name: "yy-whp-goods-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "危化品总览",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-goods-manage/yy-whp-goods-manage.vue")
          },
          {
            path: "yy-whp-put-in-manage",
            name: "yy-whp-put-in-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "入库管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-put-in-manage/yy-whp-put-in-manage.vue")
          },
          {
            path: "yy-whp-out-in-manage",
            name: "yy-whp-out-in-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "出库管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-out-in-manage/yy-whp-out-in-manage.vue")
          },
          {
            path: "yy-whp-store-list",
            name: "yy-whp-store-list",
            meta: {
              icon: "arrow-graph-up-right",
              title: "库存清单",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-store-list/yy-whp-store-list.vue")
          },
          {
            path: "yy-whp-parameter-record",
            name: "yy-whp-parameter-record",
            meta: {
              icon: "arrow-graph-up-right",
              title: "台账记录",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-parameter-record/yy-whp-parameter-record.vue")
          },
          {
            path: "yy-whp-sell-store-report",
            name: "yy-whp-sell-store-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "进销存报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-sell-store-report/yy-whp-sell-store-report.vue")
          },
          {
            path: "yy-whp-month-reports",
            name: "yy-whp-month-reports",
            meta: {
              icon: "arrow-graph-up-right",
              title: "月度结转",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-dangerouschemicals-manage/yy-whp-month-reports/yy-whp-month-reports.vue")
          }
        ]
      },
      
      
      {
        path: "yy-equipment-manage",
        name: "yy-equipment-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "设备管理",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-equipment-parameter-manage",
            name: "yy-equipment-parameter-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "设备台账",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-equipment-manage/yy-equipment-parameter-manage/yy-equipment-parameter-manage.vue")
          },
          {
            path: "yy-qrcode-manage",
            name: "yy-qrcode-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "设备二维码",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-equipment-manage/yy-qrcode-manage/yy-qrcode-manage.vue")
          },
          {
            path: "yy-equipment-warning-manage",
            name: "yy-equipment-warning-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "设备预警管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-equipment-manage/yy-equipment-warning-manage/yy-equipment-warning-manage.vue")
          },
          {
            path: "yy-dictionary-manage",
            name: "yy-dictionary-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "字典管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-equipment-manage/yy-dictionary-manage/yy-dictionary-manage.vue")
          },
        ]
      },

      {
        path: "yy-inspection-manage",
        name: "yy-inspection-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "巡检管理",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-inspection-items-manage",
            name: "yy-inspection-items-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "巡检条目管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-inspection-items-manage/yy-inspection-items-manage.vue")
          },
          {
            path: "yy-inspection-content",
            name: "yy-inspection-content",
            meta: {
              icon: "arrow-graph-up-right",
              title: "巡检内容",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-inspection-content/yy-inspection-content.vue")
          },
          {
            path: "yy-inspection-plan",
            name: "yy-inspection-plan",
            meta: {
              icon: "arrow-graph-up-right",
              title: "巡检计划",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-inspection-plan/yy-inspection-plan.vue")
          },
          {
            path: "yy-inspection-tasks",
            name: "yy-inspection-tasks",
            meta: {
              icon: "arrow-graph-up-right",
              title: "巡检任务",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-inspection-tasks/yy-inspection-tasks.vue")
          },
          {
            path: "yy-integrated-management",
            name: "yy-integrated-management",
            meta: {
              icon: "arrow-graph-up-right",
              title: "综合管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-integrated-management/yy-integrated-management.vue")
          },
          {
            path: "yy-abnormal-project-query",
            name: "yy-abnormal-project-query",
            meta: {
              icon: "arrow-graph-up-right",
              title: "异常项目查询",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-inspection-manage/yy-abnormal-project-query/yy-abnormal-project-query.vue")
          },
        ]
      },

      {
        path: "yy-knowledgebase-manage",
        name: "yy-knowledgebase-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "知识库管理",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-knowledgebase-maintenance",
            name: "yy-knowledgebase-maintenance",
            meta: {
              icon: "arrow-graph-up-right",
              title: "知识库维护",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-knowledgebase-manage/yy-knowledgebase-maintenance/yy-knowledgebase-maintenance.vue")
          },
          {
            path: "yy-knowledgebase-retrieval",
            name: "yy-knowledgebase-retrieval",
            meta: {
              icon: "arrow-graph-up-right",
              title: "知识库检索",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-knowledgebase-manage/yy-knowledgebase-retrieval/yy-knowledgebase-retrieval.vue")
          },
        ]
      },

      {
        path: "yy-announcement-manage",
        name: "yy-announcement-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "辅助",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-announcement-management",
            name: "yy-announcement-management",
            meta: {
              icon: "arrow-graph-up-right",
              title: "公告管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-announcement-manage/yy-announcement-management/yy-announcement-management.vue")
          }
        ]
      },
      
    ]
  },

  {
    path: "/main",
    name: "yy-report-management",
    meta: {
      icon: "ios-document",
      title: "报表管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "yy-energy-management-report",
        name: "yy-energy-management-report",
        meta: {
          icon: "arrow-graph-up-right",
          title: "能源管理报表",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-total-energy-consumption-report",
            name: "yy-total-energy-consumption-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "总能耗报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-management-report/yy-total-energy-consumption-report/yy-total-energy-consumption-report.vue")
          },
          {
            path: "yy-area-energy-consumption-report",
            name: "yy-area-energy-consumption-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "区域能耗报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-management-report/yy-area-energy-consumption-report/yy-area-energy-consumption-report.vue")
          },
          {
            path: "yy-department-energy-consumption-report",
            name: "yy-department-energy-consumption-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "部门能耗报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-management-report/yy-department-energy-consumption-report/yy-department-energy-consumption-report.vue")
          },
          {
            path: "yy-water-electricity-branch-report",
            name: "yy-water-electricity-branch-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "水电气分科报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-management-report/yy-water-electricity-branch-report/yy-water-electricity-branch-report.vue")
          },
          {
            path: "yy-transformer-energy-consumption-report",
            name: "yy-transformer-energy-consumption-report",
            meta: {
              icon: "arrow-graph-up-right",
              title: "变压器能耗报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-energy-management-report/yy-transformer-energy-consumption-report/yy-transformer-energy-consumption-report.vue")
          },
        ]
      },

      {
        path: "yy-maintenance-report",
        name: "yy-maintenance-report",
        meta: {
          icon: "arrow-graph-up-right",
          title: "维修报表",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-department-maintenance-workload",
            name: "yy-department-maintenance-workload",
            meta: {
              icon: "arrow-graph-up-right",
              title: "部门维修工作量统计表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-department-maintenance-workload/yy-department-maintenance-workload.vue")
          },
          {
            path: "yy-employees-maintenance-workload",
            name: "yy-employees-maintenance-workload",
            meta: {
              icon: "arrow-graph-up-right",
              title: "员工维修工作量统计表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-employees-maintenance-workload/yy-employees-maintenance-workload.vue")
          },
          {
            path: "yy-dispatch-center-workload",
            name: "yy-dispatch-center-workload",
            meta: {
              icon: "arrow-graph-up-right",
              title: "调度中心工作量统计表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-dispatch-center-workload/yy-dispatch-center-workload.vue")
          },
          {
            path: "yy-department-repair-amount",
            name: "yy-department-repair-amount",
            meta: {
              icon: "arrow-graph-up-right",
              title: "部门报修量统计表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-department-repair-amount/yy-department-repair-amount.vue")
          },
          {
            path: "yy-maintenance-satisfaction",
            name: "yy-maintenance-satisfaction",
            meta: {
              icon: "arrow-graph-up-right",
              title: "维修满意度统计表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-maintenance-satisfaction/yy-maintenance-satisfaction.vue")
          },
          {
            path: "yy-fault-category",
            name: "yy-fault-category",
            meta: {
              icon: "arrow-graph-up-right",
              title: "故障类别报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-fault-category/yy-fault-category.vue")
          },
          {
            path: "yy-logistics-month-statistical",
            name: "yy-logistics-month-statistical",
            meta: {
              icon: "arrow-graph-up-right",
              title: "一站式后勤月周报表",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-maintenance-report-manage/yy-logistics-month-statistical/yy-logistics-month-statistical.vue")
          }, 
        ]
      },
			
			{
			  path: "yy-equipmentmonitoring-report-manege",
			  name: "yy-equipmentmonitoring-report-manege",
			  meta: {
			    icon: "arrow-graph-up-right",
			    title: "设备监控报表",
			    access: ["BEHQADMIN"]
			  },
			  component: parentView,
			  children: [         
			    {
			      path: "yy-equipment-fault-report",
			      name: "yy-equipment-fault-report",
			      meta: {
			        icon: "arrow-graph-up-right",
			        title: "设备故障报表",
			        access: ["BEHQADMIN"]
			      },
			      component: parentView,
			      children: [
			        {
								path: "yy-equipment-fault-report-equipment-failure-report",
								name: "yy-equipment-fault-report-equipment-failure-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "设备故障报表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-fault-report/yy-equipment-fault-report-equipment-failure-report/yy-equipment-fault-report-equipment-failure-report.vue")
							},
							{
								path: "yy-equipment-failure-report-details",
								name: "yy-equipment-failure-report-details",
								meta: {
									icon: "arrow-graph-up-right",
									title: "设备故障报表明细表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-fault-report/yy-equipment-failure-report-details/yy-equipment-failure-report-details.vue")
							},
							{
								path: "yy-transformer-room-ower-outages-report",
								name: "yy-transformer-room-ower-outages-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "配电房停电报表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-fault-report/yy-transformer-room-ower-outages-report/yy-transformer-room-ower-outages-report.vue")
							},
			      ]
			    },
					
					{
					  path: "yy-operation-record-report",
					  name: "yy-operation-record-report",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "运行记录报表",
					    access: ["BEHQADMIN"]
					  },
					  component: parentView,
					  children: [
							{
								path: "yy-high-low-voltage-operation-record-report",
								name: "yy-high-low-voltage-operation-record-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "配电室高低压线路运行记录表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-high-low-voltage-operation-record-report/yy-high-low-voltage-operation-record-report.vue")
							},
							{
								path: "yy-surgery-room-clean-system-run-record",
								name: "yy-surgery-room-clean-system-run-record",
								meta: {
									icon: "arrow-graph-up-right",
									title: "手术室洁净系统记录",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-surgery-room-clean-system-run-record/yy-surgery-room-clean-system-run-record.vue")
							},
							{
								path: "yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report",
								name: "yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "ICU、层流病房净化设备巡视登记本",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report/yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report.vue")
							},
							{
								path: "yy-equipment-report-yy-large-medical-equipment-run-report",
								name: "yy-equipment-report-yy-large-medical-equipment-run-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "大型医疗设备运行报表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-equipment-report-yy-large-medical-equipment-run-report/yy-equipment-report-yy-large-medical-equipment-run-report.vue")
							},
							{
								path: "yy-equipment-report-yy-central-air-conditioning-run-record",
								name: "yy-equipment-report-yy-central-air-conditioning-run-record",
								meta: {
									icon: "arrow-graph-up-right",
									title: "六号楼中央空调运行记录",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-equipment-report-yy-central-air-conditioning-run-record/yy-equipment-report-yy-central-air-conditioning-run-record.vue")
							},
							{
								path: "yy-equipment-report-yy-central-air-conditioning-run-record-one",
								name: "yy-equipment-report-yy-central-air-conditioning-run-record-one",
								meta: {
									icon: "arrow-graph-up-right",
									title: "六号楼中央空调运行记录详情",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-operation-record-report/yy-equipment-report-yy-central-air-conditioning-run-record-one/yy-equipment-report-yy-central-air-conditioning-run-record-one.vue")
							},
					  ]
					},
					
					{
					  path: "yy-sewage-station-report",
					  name: "yy-sewage-station-report",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "污水站报表",
					    access: ["BEHQADMIN"]
					  },
					  component: parentView,
					  children: [
							{
								path: "yy-central-air-conditioning-run-record",
								name: "yy-central-air-conditioning-run-record",
								meta: {
									icon: "arrow-graph-up-right",
									title: "污水站排放量报表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-sewage-station-report/yy-central-air-conditioning-run-record/yy-central-air-conditioning-run-record.vue")
							},
							{
								path: "yy-sewage-station-salt-dosage-entry",
								name: "yy-sewage-station-salt-dosage-entry",
								meta: {
									icon: "arrow-graph-up-right",
									title: "污水站盐用量录入",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-sewage-station-report/yy-sewage-station-salt-dosage-entry/yy-sewage-station-salt-dosage-entry.vue")
							},
					  ]
					},
					
					{
					  path: "yy-medical-gas-report",
					  name: "yy-medical-gas-report",
					  meta: {
					    icon: "arrow-graph-up-right",
					    title: "医用气体报表",
					    access: ["BEHQADMIN"]
					  },
					  component: parentView,
					  children: [
							{
								path: "yy-equipment-report-liquid-oxygen-dosage-report",
								name: "yy-equipment-report-liquid-oxygen-dosage-report",
								meta: {
									icon: "arrow-graph-up-right",
									title: "液氧用量统计表",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-medical-gas-report/yy-equipment-report-liquid-oxygen-dosage-report/yy-equipment-report-liquid-oxygen-dosage-report.vue")
							},
							{
								path: "yy-liquid-oxygen-dosage-entry",
								name: "yy-liquid-oxygen-dosage-entry",
								meta: {
									icon: "arrow-graph-up-right",
									title: "液氧用量录入",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-medical-gas-report/yy-liquid-oxygen-dosage-entry/yy-liquid-oxygen-dosage-entry.vue")
							},
							{
								path: "yy-finance-office",
								name: "yy-finance-office",
								meta: {
									icon: "arrow-graph-up-right",
									title: "医气报表-报财务处",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-medical-gas-report/yy-finance-office/yy-finance-office.vue")
							},
							{
								path: "yy-newspapers-classics",
								name: "yy-newspapers-classics",
								meta: {
									icon: "arrow-graph-up-right",
									title: "医气报表-报经改办",
									access: ["BEHQADMIN"]
								},
								component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-medical-gas-report/yy-newspapers-classics/yy-newspapers-classics.vue")
							},
					  ]
					},
					
			  ]
			},

      // {
      //   path: "yy-equipmentmonitoring-report",
      //   name: "yy-equipmentmonitoring-report",
      //   meta: {
      //     icon: "arrow-graph-up-right",
      //     title: "设备监控报表",
      //     access: ["BEHQADMIN"]
      //   },
      //   component: parentView,
      //   children: [         
      //     {
      //       path: "yy-equipment-report",
      //       name: "yy-equipment-report",
      //       meta: {
      //         icon: "arrow-graph-up-right",
      //         title: "常规设备监控报表",
      //         access: ["BEHQADMIN"]
      //       },
      //       component: parentView,
      //       children: [         
      //         {
      //           path: "yy-equipment-report-liquid-oxygen-dosage-report",
      //           name: "yy-equipment-report-liquid-oxygen-dosage-report",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "液氧用量统计表",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-liquid-oxygen-dosage-report/yy-equipment-report-liquid-oxygen-dosage-report.vue")
      //         },
      //         {
      //           path: "yy-equipment-report-equipment-failure-report",
      //           name: "yy-equipment-report-equipment-failure-report",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "设备故障报表",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-equipment-failure-report/yy-equipment-report-equipment-failure-report.vue")
      //         },
      //         {
      //           path: "yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report",
      //           name: "yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "ICU、层流病房净化设备巡视登记本",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report/yy-equipment-report-yy-ICU-ward-purification-equipment-registration-report.vue")
      //         },
      //         {
      //           path: "yy-equipment-report-yy-central-air-conditioning-run-record",
      //           name: "yy-equipment-report-yy-central-air-conditioning-run-record",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "六号楼中央空调运行记录",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-yy-central-air-conditioning-run-record/yy-equipment-report-yy-central-air-conditioning-run-record.vue")
      //         },
      //         {
      //           path: "yy-equipment-report-yy-large-medical-equipment-run-report",
      //           name: "yy-equipment-report-yy-large-medical-equipment-run-report",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "大型医疗设备运行报表",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-yy-large-medical-equipment-run-report/yy-equipment-report-yy-large-medical-equipment-run-report.vue")
      //         },
      //         {
      //           path: "yy-equipment-report-yy-sewage-station-emissions-report",
      //           name: "yy-equipment-report-yy-sewage-station-emissions-report",
      //           meta: {
      //             icon: "arrow-graph-up-right",
      //             title: "污水站排放量报表",
      //             access: ["BEHQADMIN"]
      //           },
      //           component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-equipment-report/yy-equipment-report-yy-sewage-station-emissions-report/yy-equipment-report-yy-sewage-station-emissions-report.vue")
      //         },
      //       ]
      //     },
      //     {
      //       path: "yy-surgery-room-clean-system-run-record",
      //       name: "yy-surgery-room-clean-system-run-record",
      //       meta: {
      //         icon: "arrow-graph-up-right",
      //         title: "手术室洁净系统记录",
      //         access: ["BEHQADMIN"]
      //       },
      //       component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-surgery-room-clean-system-run-record/yy-surgery-room-clean-system-run-record.vue")
      //     },
      //     {
      //       path: "yy-liquid-oxygen-dosage-entry",
      //       name: "yy-liquid-oxygen-dosage-entry",
      //       meta: {
      //         icon: "arrow-graph-up-right",
      //         title: "液氧用量录入",
      //         access: ["BEHQADMIN"]
      //       },
      //       component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-liquid-oxygen-dosage-entry/yy-liquid-oxygen-dosage-entry.vue")
      //     },
      //     {
      //       path: "yy-sewage-station-salt-dosage-entry",
      //       name: "yy-sewage-station-salt-dosage-entry",
      //       meta: {
      //         icon: "arrow-graph-up-right",
      //         title: "污水站盐用量录入",
      //         access: ["BEHQADMIN"]
      //       },
      //       component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-sewage-station-salt-dosage-entry/yy-sewage-station-salt-dosage-entry.vue")
      //     },
      //     {
      //       path: "yy-transformer-room-ower-outages-report",
      //       name: "yy-transformer-room-ower-outages-report",
      //       meta: {
      //         icon: "arrow-graph-up-right",
      //         title: "配电房停电报表",
      //         access: ["BEHQADMIN"]
      //       },
      //       component: () => import("@/view/yy-equipmentmonitoring-report-manege/yy-transformer-room-ower-outages-report/yy-transformer-room-ower-outages-report.vue")
      //     },
      //   ]
      // },

    ]
  },
  
  {
    path: "/main",
    name: "yy-system-management",
    meta: {
      icon: "md-cog",
      title: "系统管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "yy-cantubeconfig-manage",
        name: "yy-cantubeconfig-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "能管配置",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-wyGDP",
            name: "yy-wyGDP",
            meta: {
              icon: "arrow-graph-up-right",
              title: "万元GDP",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-cantubeconfig-manage/yy-wyGDP/yy-wyGDP.vue")
          },
          {
            path: "yy-see-doctor-number",
            name: "yy-see-doctor-number",
            meta: {
              icon: "arrow-graph-up-right",
              title: "就诊人数",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-cantubeconfig-manage/yy-see-doctor-number/yy-see-doctor-number.vue")
          },
        ]
      },

      {
        path: "yy-operationalconfig-manage",
        name: "yy-operationalconfig-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "运维配置",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-employees-manage",
            name: "yy-employees-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "员工管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-employees-manage/yy-employees-manage.vue")
          },
          {
            path: "yy-role-manage",
            name: "yy-role-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "角色管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-role-manage/yy-role-manage.vue")
          },

          {
            path: "yy-department-manage",
            name: "yy-department-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "部门管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-department-manage/yy-department-manage.vue")
          },
          {
            path: "yy-warehouse-manage",
            name: "yy-warehouse-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "仓库管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-warehouse-manage/yy-warehouse-manage.vue")
          },
          {
            path: "yy-warehouse-permissions-manage",
            name: "yy-warehouse-permissions-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "仓库权限管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-warehouse-permissions-manage/yy-warehouse-permissions-manage.vue")
          },
          {
            path: "yy-maintenance-time-manage",
            name: "yy-maintenance-time-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "维修工时管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-maintenance-time-manage/yy-maintenance-time-manage.vue")
          },
          {
            path: "yy-maintenance-projects",
            name: "yy-maintenance-projects",
            meta: {
              icon: "arrow-graph-up-right",
              title: "维修项目",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-maintenance-projects/yy-maintenance-projects.vue")
          },
          {
            path: "yy-place-manage",
            name: "yy-place-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "地点管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-place-manage/yy-place-manage.vue")
          },
          {
            path: "yy-phone-manage",
            name: "yy-phone-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "电话管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-operationalconfig-manage/yy-phone-manage/yy-phone-manage.vue")
          },
        ]
      },

      {
        path: "yy-newsconfig-manage",
        name: "yy-newsconfig-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "新闻配置",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-programa-manage",
            name: "yy-programa-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "栏目管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-newsconfig-manage/yy-programa-manage/yy-programa-manage.vue")
          },
          {
            path: "yy-news-manage",
            name: "yy-news-manage",
            meta: {
              icon: "arrow-graph-up-right",
              title: "新闻管理",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-newsconfig-manage/yy-news-manage/yy-news-manage.vue")
          },
        ]
      },

      {
        path: "yy-userinformation-manage",
        name: "yy-userinformation-manage",
        meta: {
          icon: "arrow-graph-up-right",
          title: "用户信息",
          access: ["BEHQADMIN"]
        },
        component: parentView,
        children: [
          {
            path: "yy-login-log",
            name: "yy-login-log",
            meta: {
              icon: "arrow-graph-up-right",
              title: "登录日志",
              access: ["BEHQADMIN"]
            },
            component: () => import("@/view/yy-userinformation-manage/yy-login-log/yy-login-log.vue")
          },
        ]
      }
    ]
  },

  {
    path: "/main",
    name: "yy-alarm-management",
    meta: {
      icon: "md-notifications",
      title: "报警管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
      children: [
        {
          path: "/yy-alarm-management-manage/yy-untreated-alarm",
          name: "yy-untreated-alarm",
          meta: {
            icon: "arrow-graph-up-right",
            title: "未处理报警",
            access: ["BEHQADMIN"]
          },
          component: () => import("@/view/yy-alarm-management-manage/yy-untreated-alarm/yy-untreated-alarm.vue")
        },
        {
          path: "/yy-alarm-management-manage/yy-historical-alarm",
          name: "yy-historical-alarm",
          meta: {
            icon: "arrow-graph-up-right",
            title: "历史报警",
            access: ["BEHQADMIN"]
          },
          component: () => import("@/view/yy-alarm-management-manage/yy-historical-alarm/yy-historical-alarm.vue")
        },
        {
          path: "/yy-alarm-management-manage/yy-camera-alarm",
          name: "yy-camera-alarm",
          meta: {
            icon: "arrow-graph-up-right",
            title: "摄像头报警",
            access: ["BEHQADMIN"]
          },
          component: () => import("@/view/yy-alarm-management-manage/yy-camera-alarm/yy-camera-alarm.vue")
        },
      ]
  },

  {
    path: "/main",
    name: "yy-diagnostic-management",
    meta: {
      icon: "md-medkit",
      title: "诊断管理",
      showAlways: true,
      access: ["BEHQADMIN"]
    },
    component: Main,
    children: [
      {
        path: "/yy-diagnostic-management/yy-key-diagnostic",
        name: "yy-key-diagnostic",
        meta: {
          icon: "arrow-graph-up-right",
          title: "一键诊断",
          access: ["BEHQADMIN"]
        },
        component: () => import("@/view/yy-diagnostic-management/yy-key-diagnostic/yy-key-diagnostic.vue")
      },
    ]
  }, 
  
];

let routers = configRouter.concat(permissionConfigRouter);
router.addRoutes(routers);

let access = getCookieObj("access");
if (!access || access == "undefined") {
  store.commit('setRouter',routers);
}

router.beforeEach((to, from, next) => {
  router.customBeforeEach(to, from, next, store.state.app.routers);
});

router.afterEach(to => {
  router.customAfterEach(to);
});

export default router;
