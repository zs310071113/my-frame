import Vue from "vue";
import Vuex from "vuex";

import user from "./module/user";
import app from "./module/app";

Vue.use(Vuex);

let store = new Vuex.Store({});
store.registerModule("user", user);
store.registerModule("app", app);

export default store;
