import {
  getBreadCrumbList,
  setTagNavListInLocalstorage,
  getMenuByRouter,
  getTagNavListFromLocalstorage,
  getHomeRoute,
  getSessionObjStore,
  setSessionObjStore,
  setLocalObjStore,
  getLocalObjStore
} from "@/libs/util";

export default {
  state: {
    breadCrumbList: [],
    tagNavList: [],
    homeRoute: [],
    local: "",
    routers: getLocalObjStore("routers") ? getLocalObjStore("routers") : []
  },
  getters: {
    menuList: (state, getters, rootState) => {
      return getMenuByRouter(state.routers, rootState.user.access);
    }
  },
  mutations: {
    setBreadCrumb(state, routeMetched) {
      state.breadCrumbList = getBreadCrumbList(routeMetched);
    },
    setTagNavList(state, list) {
      if (list) {
        state.tagNavList = [...list];
        setTagNavListInLocalstorage([...list]);
      } else state.tagNavList = getTagNavListFromLocalstorage();
    },
    addTag(state, item, type = "unshift") {
      if (item.meta && item.meta.notTag) {
        return;
      }
      if (state.tagNavList.findIndex(tag => tag.name === item.name) < 0) {
        if (type === "push") state.tagNavList.push(item);
        else state.tagNavList.unshift(item);
        setTagNavListInLocalstorage([...state.tagNavList]);
      }
    },
    setLocal(state, lang) {
      state.local = lang;
    },
    setRouter(state, routers) {
      state.routers = routers;
      state.homeRoute = getHomeRoute(routers);
      setLocalObjStore("routers", routers);
    }
  }
};
