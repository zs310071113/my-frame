import * as API from "@/api";
import {
  getSessionObjStore,
  setSessionObjStore,
  setSessionStore,
  getSessionStore,
  setCookie,
  getCookie,
  setCookieObj,
  getCookieObj
} from "@/libs/util";

export default {
  state: {
    userInfo: getCookieObj("userInfo") ? getCookieObj("userInfo") : "",
    token: getCookie("token"),
    access: getCookieObj("access"),
    messageCount: 0
  },
  mutations: {
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo;
      setCookieObj("userInfo", userInfo);
    },
    setAccess(state, access) {
      state.access = access;
      setCookieObj("access", access);
    },
    setToken(state, token) {
      state.token = token;
      setCookie("token", token);
    }
  },
  actions: {
    // 登录
    handleLogin({ state, commit }, obj) {
      return new Promise((resolve, reject) => {
        API.login(obj)
          .then(res => {
            const data = res.data;
            //commit("setToken", data.userName);
            //commit("setUserInfo", data);
            //commit("setAccess", data.access.toString());
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    // 退出登录
    handleLogOut({ state, commit }) {
      return new Promise((resolve, reject) => {
        API.logout(state.token)
          .then(() => {
            commit("setUserInfo", "");
            commit("setToken", "");
            commit("setAccess", "");
            //------------------------------
            commit("setRouter", []);
            commit("setTagNavList", []);
            commit("breadCrumbList", []);
            commit("homeRoute", []);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    // 客户端单方面退出
    handleClientLogOut({ state, commit }) {
      commit("setUserInfo", "");
      commit("setToken", "");
      commit("setAccess", "");
      //------------------------------
      commit("setRouter", []);
      commit("setTagNavList", []);
      commit("breadCrumbList", []);
      commit("homeRoute", []);
    },
    // 修改密码
    editUserPw({ state, commit }, obj) {
      return new Promise((resolve, reject) => {
        API.editPW(obj)
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    }
  }
};
