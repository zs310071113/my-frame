const path = require("path");
const fs = require("fs");
//const shell = require('shelljs');
const commandOpt = process.env.npm_config_argv;
let site = 'default';
let port = 8888;
if (commandOpt) {
  const argv = JSON.parse(commandOpt);
  const arr = argv.original;
  let type = arr[1];
  if (type == 'serve' || type == 'build') {
    site = arr[2] && arr[2].replace("--", "") || 'default';
    port = arr[3] && arr[3].replace("--", "") || 8888;
    console.log('start:'+type+'--'+site);
  }
}
const resolve = dir => {
  return path.join(__dirname, dir);
};
//shell.echo('Delete the last packaged code on '+ site);
//shell.rm('-rf', "./dist/" + site + "/");

let proxyObj = {

  'default':{
    /*默认系统接口*/
    target: 'http://hqn.fun/',
    pathRewrite: {'^/api': ''},
    ws: true,
    changeOrigin: true,
    secure: false
  },
  
  //洗涤领域
  'xd':{
    target: 'http://hqn.fun:16811/',
    pathRewrite: {'^/xd/api': ''},
    ws: true,
    changeOrigin: true,
    secure: false
  },
  
  //医院领域
  'yy': {
    //重庆儿童医院正式接口
    //target: 'http://211.100.28.180/BEEWebAPIYWChCMU/',
    //pathRewrite: {'^/api': '/api'},
    //target: 'http://211.100.28.180/BEEWebAPIYWChCMU/',
    //pathRewrite: {'^/api': '/api'},
    /*重庆儿童医院测试接口*/
    //target: 'http://218.70.30.134:32537/beewebapitest',
    //pathRewrite: {'^/api': '/api'},
    /*重庆儿童医院mock接口*/
    //target: 'https://easy-mock.com/mock/5be1a70d19266809d2d80bf7',
    target: 'http://hqn.fun:9996/mock/5c1ca4c512eca627d8bc1014',
    pathRewrite: {'^/yy/api': '/api'},
    ws: true,
    changeOrigin: true,
    secure: false
  },
  
   //洗涤领域
  'ylyxd':{
    target: 'http://hqn.fun:16811/',
    pathRewrite: {'^/xd/ylyxd/api': ''},
    ws: true,
    changeOrigin: true,
    secure: false
  },
  
  //医院领域
  'cqykdxfsetyy': {
    //重庆儿童医院正式接口
    //target: 'http://211.100.28.180/BEEWebAPIYWChCMU/',
    //pathRewrite: {'^/api': '/api'},
    //target: 'http://211.100.28.180/BEEWebAPIYWChCMU/',
    //pathRewrite: {'^/api': '/api'},
    /*重庆儿童医院测试接口*/
    //target: 'http://218.70.30.134:32537/beewebapitest',
    //pathRewrite: {'^/api': '/api'},
    /*重庆儿童医院mock接口*/
    //target: 'https://easy-mock.com/mock/5be1a70d19266809d2d80bf7',
    target: 'http://hqn.fun:9996/mock/5c1ca4c512eca627d8bc1014',
    pathRewrite: {'^/yy/cqykdxfsetyy/api': '/api'},
    ws: true,
    changeOrigin: true,
    secure: false
  }
  
  
};

module.exports = {

  baseUrl: "/",
  
  //publicPath: "/",

  chainWebpack: config => {

    config.resolve.alias
      .set("@", resolve("src"))
      .set("_c", resolve("src/components"))
      .set("_conf", resolve("config"))
      .end();
    
    config.module
      .rule("images")
      .test(/\.(gif|png|jpe?g|svg)$/i)
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 1000,
        name: "img/[name].[hash:7].[ext]",
        publicPath: "/"
      })
      .end();
    
    config.module
      .rule("images-min")
      .test(/\.(gif|png|jpe?g|svg)$/i)
      .use("image-webpack-loader")
      .loader("image-webpack-loader")
      .options({
        mozjpeg: {
          progressive: true,
          quality: 80
        },
        optipng: {
          enabled: false
        },
        pngquant: {
          quality: "80-90",
          speed: 8
        },
        gifsicle: {
          interlaced: false
        },
        /* Compress JPG & PNG images into WEBP
        webp: {
          quality: 80
        }
        */
      })
      .end();

    config
      .plugin("html")
      .tap(args => {
        args[0].template = "./src/site/" + site + "/index.html",
        args[0].favicon = "./src/site/" + site + "/favicon.ico"
        return args;
      })
      .end();
  },

  //lintOnSave: true, 
  
  outputDir: "./dist/" + site + "/",

  configureWebpack: {
    entry: "./src/site/" + site + "/main.js",
    externals: {
      "vue": "Vue",
      "iview": "iview",
      "vue-router": "VueRouter",
      "vuex": "Vuex",
      "axios": "axios",
      "echarts": "echarts",
      "lodash":"_",
      "vue-i18n":"VueI18n"
    },
  },

  productionSourceMap: false,
  
  //兼容 转义node中的iview const/let语法. (safair < 10)
  transpileDependencies: ['iview/src/*'],

  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [path.resolve(__dirname, "./src/styles/global.less")]
    },
    "uglifyjs-webpack-plugin": {
      uglifyOptions: {
        compress: {
          warnings: false,
          drop_console: true,
          collapse_vars: true,
          reduce_vars: true
        },
        output: {
          beautify: false,
          comments: false
        }
      }
    }
  },

  css: {
    loaderOptions: {
      css: {
        localIdentName: "[name]-[hash]",
        camelCase: "only"
      }
    }
  },

  devServer: {
    port: port,
    proxy: {
      '/xd/ylyxd/api/': proxyObj['ylyxd'],
      '/yy/cqykdxfsetyy/api/': proxyObj['cqykdxfsetyy']
    }
  }
};


